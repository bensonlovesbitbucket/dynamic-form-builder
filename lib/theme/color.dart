// Color

import 'package:flutter/material.dart';

const kPrimaryColor = const Color(0xFFff6666);
const kSecondaryColor = const Color(0xFF39d2d4);
const kBackgroundColor = const Color(0xFFf3f4f5);
const kLoginSubTextColor = const Color(0xFF9b9b9b);
const kGreyTextColor = const Color(0xFF9b9b9b);
const kProfileEditFieldColorWithoutTransaparent =
    Color.fromARGB(255, 226, 228, 231);
const kProfileEditFieldColorWithSemiTransaparent =
    Color.fromARGB(100, 226, 228, 231);
const kSignUpBackgroundColor = const Color.fromARGB(255, 243, 244, 245);
const kEditTextFormFieldContainerHeight = 50.0;
const kBookingListCardBorderRadius = 10.0;
const bgColor = Color.fromRGBO(240, 241, 244, 1);
