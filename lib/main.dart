import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_builder/config/constant.dart';
import 'package:form_builder/config/routes.dart';
import 'package:form_builder/screens/create_screen/create.dart';
import 'package:form_builder/screens/create_screen/create_bloc.dart';
import 'package:form_builder/screens/splash_screen.dart';
import 'package:form_builder/theme/color.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  if (kDebugMode) {
    runApp(
      DevicePreview(
        enabled: kDebugMode,
        builder: (context) => MyApp(),
      ),
    );
  } else {
    runApp(MyApp());
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) {
            return CreateBloc();
          },
        ),
      ],
      child: MaterialApp(
        title: appName,
        theme: ThemeData(primaryColor: kPrimaryColor),
        debugShowCheckedModeBanner: false,
        home: CreateScreen(),
        initialRoute: '/',
        routes: routes,
      ),
    );
  }
}
