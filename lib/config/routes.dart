import 'package:flutter/material.dart';
import 'package:form_builder/config/constant.dart';
import 'package:form_builder/screens/create_screen/create.dart';
import 'package:form_builder/screens/preview_screen/preview.dart';

final Map<String, WidgetBuilder> routes = {
  createScreenRouteName: (context) => CreateScreen(),
  previewScreenRouteName: (context) => PreviewScreen(
        questionList: [],
      )
};
