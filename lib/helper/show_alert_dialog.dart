import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:form_builder/helper/size_config.dart';
import 'package:form_builder/theme/color.dart';

class ShowAlertDialog extends StatelessWidget {
  final String header;

  const ShowAlertDialog({required this.header});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(
              header,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            width: SizeConfig.blockSizeHorizontal * 10,
            height: SizeConfig.blockSizeVertical * 10,
            child: PlatformCircularProgressIndicator(
                material: (_, __) => MaterialProgressIndicatorData(
                    valueColor: AlwaysStoppedAnimation<Color>(kPrimaryColor))),
          ),
        ],
      ),
    );
  }
}
