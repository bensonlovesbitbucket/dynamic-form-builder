class CheckBoxModel {
  String option;

  CheckBoxModel([
    this.option = "",
  ]);

  set setOption(String option) => this.option = option;

  @override
  String toString() {
    return "Option: " + option;
  }
}
