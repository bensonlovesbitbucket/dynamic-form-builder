// enum ElementType { Text, Number, Toggle, Checkbox }

/// Form element type
///
/// 1 = Text
/// 2 = Number
/// 3 = Toggle
/// 4 = Checkbox
///
///
class QuestionModel {
  String answerType;
  String question;
  bool isRequired;
  List<String> checkBoxOption;

  QuestionModel(
      {required this.answerType,
      required this.question,
      required this.isRequired,
      required this.checkBoxOption});

  set setElementType(String elementType) => this.answerType = elementType;
  set setQuestion(String question) => this.question = question;
  set setIsRequired(bool isRequired) => this.isRequired = isRequired;
  set setCheckBoxOption(List<String> checkBoxOption) =>
      this.checkBoxOption = List.from(checkBoxOption);

  @override
  String toString() {
    String checkBoxString = "";
    if (checkBoxOption.isNotEmpty) {
      checkBoxOption.forEach((element) {
        checkBoxString = checkBoxString + " - " + element;
      });
    }
    return "Answer Type: " +
        answerType +
        "  | Question: " +
        question +
        " | Required: " +
        isRequired.toString() +
        " | Checkbox option: " +
        checkBoxString;
    checkBoxString = "";
  }
}
