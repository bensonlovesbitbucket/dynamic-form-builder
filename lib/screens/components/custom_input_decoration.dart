import 'package:flutter/material.dart';

InputDecoration customInputDecoration(String label) {
  return InputDecoration(
      labelText: label,
      border: InputBorder.none,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      errorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      contentPadding: EdgeInsets.only(right: 5));
}
