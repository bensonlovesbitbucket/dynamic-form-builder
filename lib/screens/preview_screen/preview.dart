import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder/helper/size_config.dart';
import 'package:form_builder/model/question_model.dart';
import 'package:form_builder/screens/components/custom_input_decoration.dart';
import 'package:form_builder/screens/create_screen/components/question.dart';
import 'package:form_builder/theme/color.dart';

class PreviewScreen extends StatefulWidget {
  final List<QuestionModel> questionList;
  const PreviewScreen({required this.questionList});

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {
  final _previewFormKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
            bottom: false,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: FormBuilder(
                      key: _previewFormKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: ListView.builder(
                        itemCount: widget.questionList.length,
                        itemBuilder: (context, index) {
                          return Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Question " +
                                          (index + 1).toString() +
                                          ": " +
                                          widget.questionList[index].question,
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.safeBlockHorizontal *
                                                  7),
                                    ),
                                  ),
                                  (() {
                                    late Widget answerType;

                                    switch (
                                        widget.questionList[index].answerType) {
                                      case "1":
                                        answerType = FormBuilderTextField(
                                          name: 'answer_' + index.toString(),
                                          validator: widget.questionList[index]
                                                  .isRequired
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context),
                                                  FormBuilderValidators.max(
                                                      context, 200)
                                                ])
                                              : FormBuilderValidators.compose([
                                                  FormBuilderValidators.max(
                                                      context, 200),
                                                ]),
                                        );

                                        break;

                                      case "2":
                                        answerType = FormBuilderTextField(
                                          name: 'answer_' + index.toString(),
                                          validator: widget.questionList[index]
                                                  .isRequired
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context),
                                                  FormBuilderValidators.numeric(
                                                      context),
                                                  FormBuilderValidators.max(
                                                      context, 200)
                                                ])
                                              : FormBuilderValidators.compose([
                                                  FormBuilderValidators.numeric(
                                                      context),
                                                  FormBuilderValidators.max(
                                                      context, 200),
                                                ]),
                                        );
                                        break;

                                      case "3":
                                        answerType = FormBuilderSwitch(
                                          name: 'answer_' + index.toString(),
                                          title: Text("switch"),
                                          initialValue: false,
                                          decoration: customInputDecoration(""),
                                          validator: widget.questionList[index]
                                                  .isRequired
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : FormBuilderValidators.compose(
                                                  []),
                                        );
                                        break;

                                      case "4":
                                        answerType = FormBuilderCheckboxGroup(
                                          name: 'answer_' + index.toString(),
                                          options: widget.questionList[index]
                                              .checkBoxOption
                                              .map((answer) =>
                                                  FormBuilderFieldOption(
                                                      value: answer))
                                              .toList(growable: false),
                                          decoration: customInputDecoration(""),
                                        );
                                        break;

                                      default:
                                        break;
                                    }
                                    return answerType;
                                  }()),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _previewFormKey.currentState!.validate();
                  },
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: double.infinity,
                      height: SizeConfig.safeBlockVertical * 9,
                      child: Center(
                          child: Text(
                        "Submit",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockVertical * 4),
                      )),
                      color: kSecondaryColor,
                    ),
                  ),
                )
              ],
            )));
  }
}

// class _PreviewScreenState extends State<PreviewScreen> {
//   final _formKey = GlobalKey<FormBuilderState>();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: Column(
//           children: <Widget>[
//             FormBuilder(
//               key: _formKey,
//               autovalidateMode: AutovalidateMode.always,
//               child: Column(
//                 children: [
//                   FormBuilderTextField(
//                     name: 'age',
//                     decoration: InputDecoration(
//                       labelText:
//                           'This value is passed along to the [Text.maxLines] attribute of the [Text] widget used to display the hint text.',
//                     ),
//                     onChanged: (value) {},
//                     // valueTransformer: (text) => num.tryParse(text),
//                     validator: FormBuilderValidators.compose([
//                       FormBuilderValidators.required(context),
//                       FormBuilderValidators.numeric(context),
//                       FormBuilderValidators.max(context, 70),
//                     ]),
//                     keyboardType: TextInputType.number,
//                   ),
//                 ],
//               ),
//             ),
//             Row(
//               children: <Widget>[
//                 Expanded(
//                   child: MaterialButton(
//                     color: Theme.of(context).accentColor,
//                     child: Text(
//                       "Submit",
//                       style: TextStyle(color: Colors.white),
//                     ),
//                     onPressed: () {
//                       _formKey.currentState?.save();
//                       if (_formKey.currentState!.validate()) {
//                         print(_formKey.currentState?.value);
//                       } else {
//                         print("validation failed");
//                       }
//                     },
//                   ),
//                 ),
//                 SizedBox(width: 20),
//                 Expanded(
//                   child: MaterialButton(
//                     color: Theme.of(context).accentColor,
//                     child: Text(
//                       "Reset",
//                       style: TextStyle(color: Colors.white),
//                     ),
//                     onPressed: () {
//                       _formKey.currentState?.reset();
//                     },
//                   ),
//                 ),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
