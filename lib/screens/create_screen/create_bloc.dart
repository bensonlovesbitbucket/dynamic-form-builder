import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_builder/screens/create_screen/create_event.dart';
import 'package:form_builder/screens/create_screen/create_state.dart';

class CreateBloc extends Bloc<CreateEvent, CreateState> {
  CreateBloc() : super(CreateCheckBoxInitialized());

  @override
  Stream<CreateState> mapEventToState(
    CreateEvent event,
  ) async* {
    try {
      if (event is CreateCheckBoxRowEvent) {
        yield CreateCheckBoxRowSuccess(
            questionIndex: event.questionIndex,
            checkboxIndex: event.checkBoxRowindex);
      } else if (event is CreateQuestionEvent) {
        yield CreateQuestionSuccess();
      } else if (event is RemoveQuestionEvent) {
        yield RemoveQuestionSuccess(index: event.index);
      } else if (event is UpdateQuestionEvent) {
        yield UpdateQuestionSuccess(
            index: event.index, question: event.question);
      } else if (event is UpdateAnswerTypeEvent) {
        yield UpdateAnswerTypeSuccess(
            index: event.index, answerType: event.answerType);
      } else if (event is UpdateRequiredEvent) {
        yield UpdateRequiredSuccess(
            index: event.index, isRequired: event.isRequired);
      } else if (event is UpdateCheckBoxEvent) {
        yield UpdateCheckBoxSuccess(
            questionIndex: event.questionIndex,
            checkboxIndex: event.checkBoxRowIndex,
            checkboxText: event.checkBoxRowText);
      } else if (event is QuestionIsEmptyEvent) {
        yield QuestionIsEmptySuccess(index: event.index);
      } else if (event is AnswerTypeIsEmptyEvent) {
        yield AnswerTypeIsEmptySuccess(index: event.index);
      }
    } catch (e) {}
  }
}
