import 'package:form_builder/model/question_model.dart';

abstract class CreateState {}

class CreateCheckBoxInitialized extends CreateState {}

class CreateCheckBoxRowSuccess extends CreateState {
  final int questionIndex;
  final int checkboxIndex;
  CreateCheckBoxRowSuccess(
      {required this.questionIndex, required this.checkboxIndex});
}

class UpdateCheckBoxSuccess extends CreateState {
  final int questionIndex;
  final int checkboxIndex;
  final String checkboxText;
  UpdateCheckBoxSuccess(
      {required this.questionIndex,
      required this.checkboxIndex,
      required this.checkboxText});
}

class CreateQuestionSuccess extends CreateState {
  CreateQuestionSuccess();
}

class RemoveQuestionSuccess extends CreateState {
  final int index;
  RemoveQuestionSuccess({required this.index});
}

class UpdateQuestionSuccess extends CreateState {
  final int index;
  String question;
  UpdateQuestionSuccess({required this.index, required this.question});
}

class UpdateAnswerTypeSuccess extends CreateState {
  final int index;
  String answerType;
  UpdateAnswerTypeSuccess({required this.index, required this.answerType});
}

class UpdateRequiredSuccess extends CreateState {
  final int index;
  final bool isRequired;
  UpdateRequiredSuccess({required this.index, required this.isRequired});
}

class QuestionIsEmptySuccess extends CreateState {
  final List<int> index;
  QuestionIsEmptySuccess({required this.index});
}

class AnswerTypeIsEmptySuccess extends CreateState {
  final List<int> index;
  AnswerTypeIsEmptySuccess({required this.index});
}
