import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_builder/config/constant.dart';
import 'package:form_builder/helper/size_config.dart';
import 'package:form_builder/model/question_model.dart';
import 'package:form_builder/screens/components/custom_input_decoration.dart';
import 'package:form_builder/screens/create_screen/components/create_check_box.dart';
import 'package:form_builder/screens/create_screen/components/question.dart';
import 'package:form_builder/screens/create_screen/create_bloc.dart';
import 'package:form_builder/screens/create_screen/create_event.dart';
import 'package:form_builder/screens/create_screen/create_state.dart';
import 'package:form_builder/screens/preview_screen/preview.dart';
import 'package:form_builder/theme/color.dart';

class CreateScreen extends StatefulWidget {
  const CreateScreen({Key? key}) : super(key: key);

  @override
  _CreateScreenState createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> {
  List<Widget> addedQuestionWidget = [
    Question(
        index: 0,
        questionModel: QuestionModel(
            answerType: "",
            question: "",
            isRequired: false,
            checkBoxOption: []))
  ];

  _addWidget() {
    setState(() {
      addedQuestionWidget.add(Question(
          index: addedQuestionWidget.length,
          questionModel: QuestionModel(
              answerType: "",
              question: "",
              isRequired: false,
              checkBoxOption: [])));
    });
  }

  late CreateBloc _createBloc;

  @override
  void initState() {
    super.initState();
    _createBloc = BlocProvider.of<CreateBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BlocConsumer(
      bloc: _createBloc,
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              appName,
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      right: SizeConfig.safeBlockHorizontal * 3),
                  child: GestureDetector(
                    onTap: () {
                      _addWidget();
                    },
                    child: Icon(Icons.plus_one),
                  )),
              Padding(
                  padding: EdgeInsets.only(
                      right: SizeConfig.safeBlockHorizontal * 3),
                  child: GestureDetector(
                    onTap: () {
                      List<int> emptyQuestion = [];
                      List<int> emptyAnswer = [];
                      List<QuestionModel> addedQuestionModel = [];
                      addedQuestionWidget.asMap().forEach((index, element) {
                        Question question = element as Question;

                        if (question.questionModel.question.isEmpty) {
                          emptyQuestion.add(index);
                        }
                        if (question.questionModel.answerType.isEmpty) {
                          emptyAnswer.add(index);
                        }
                        addedQuestionModel.add(question.questionModel);

                        print("Question index: " +
                            question.index.toString() +
                            " " +
                            "Question model: " +
                            question.questionModel.toString());
                      });
                      if (emptyQuestion.isNotEmpty) {
                        _createBloc
                            .add(QuestionIsEmptyEvent(index: emptyQuestion));
                      }
                      if (emptyAnswer.isNotEmpty) {
                        _createBloc
                            .add(AnswerTypeIsEmptyEvent(index: emptyAnswer));
                      }
                      if (addedQuestionModel.isNotEmpty) {
                        if (emptyQuestion.isEmpty && emptyAnswer.isEmpty) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PreviewScreen(
                                questionList: addedQuestionModel,
                              ),
                            ),
                          );
                        }
                      } else {
                        Fluttertoast.showToast(
                            msg: "Please create at least 1 Question",
                            toastLength: Toast.LENGTH_LONG);
                      }
                    },
                    child: Icon(Icons.preview),
                  )),
            ],
          ),
          body: SafeArea(
            child: LayoutBuilder(
              builder: (context, constraint) {
                return SingleChildScrollView(
                  child: Column(children: addedQuestionWidget),
                );
                // return SingleChildScrollView(
                //     child: ConstrainedBox(
                //   constraints: BoxConstraints(minHeight: constraint.maxHeight),
                //   child: IntrinsicHeight(
                //     child: Column(children: addedQuestionWidget),
                //   ),
                // ));
              },
            ),
          ),
        );
      },
      listener: (BuildContext context, Object? state) {
        if (state is RemoveQuestionSuccess) {
          setState(() {
            addedQuestionWidget.removeAt(state.index);
          });
        } else if (state is UpdateQuestionSuccess) {
          Question question =
              addedQuestionWidget.elementAt(state.index) as Question;
          setState(() {
            question.questionModel.question = state.question;
          });
        } else if (state is UpdateAnswerTypeSuccess) {
          Question question =
              addedQuestionWidget.elementAt(state.index) as Question;
          setState(() {
            question.questionModel.answerType = state.answerType;
          });
        } else if (state is UpdateRequiredSuccess) {
          setState(() {
            Question question =
                addedQuestionWidget.elementAt(state.index) as Question;
            question.questionModel.isRequired = state.isRequired;
          });
        } else if (state is CreateCheckBoxRowSuccess) {
          setState(() {
            Question question =
                addedQuestionWidget.elementAt(state.questionIndex) as Question;
            question.questionModel.checkBoxOption
                .insert(state.checkboxIndex, "");
          });
        } else if (state is UpdateCheckBoxSuccess) {
          setState(() {
            Question question =
                addedQuestionWidget.elementAt(state.questionIndex) as Question;
            question.questionModel.checkBoxOption[state.checkboxIndex] =
                state.checkboxText;
          });
        }
      },
    );
  }
}
