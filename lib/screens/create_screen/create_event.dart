import 'package:form_builder/model/question_model.dart';

abstract class CreateEvent {}

class CreateCheckBoxRowEvent extends CreateEvent {
  final int questionIndex;
  final int checkBoxRowindex;
  final String checkBoxText;
  CreateCheckBoxRowEvent(
      {required this.questionIndex,
      required this.checkBoxRowindex,
      required this.checkBoxText})
      : super();
}

class UpdateCheckBoxEvent extends CreateEvent {
  final int questionIndex;
  final int checkBoxRowIndex;
  final String checkBoxRowText;
  UpdateCheckBoxEvent(
      {required this.questionIndex,
      required this.checkBoxRowIndex,
      required this.checkBoxRowText})
      : super();
}

class CreateQuestionEvent extends CreateEvent {
  CreateQuestionEvent() : super();
}

class RemoveQuestionEvent extends CreateEvent {
  final int index;
  RemoveQuestionEvent({required this.index}) : super();
}

class UpdateQuestionEvent extends CreateEvent {
  final int index;
  final String question;
  UpdateQuestionEvent({required this.index, required this.question}) : super();
}

class UpdateAnswerTypeEvent extends CreateEvent {
  final int index;
  final String answerType;
  UpdateAnswerTypeEvent({required this.index, required this.answerType})
      : super();
}

class UpdateRequiredEvent extends CreateEvent {
  final int index;
  final bool isRequired;
  UpdateRequiredEvent({required this.index, required this.isRequired})
      : super();
}

class QuestionIsEmptyEvent extends CreateEvent {
  final List<int> index;
  QuestionIsEmptyEvent({required this.index}) : super();
}

class AnswerTypeIsEmptyEvent extends CreateEvent {
  final List<int> index;
  AnswerTypeIsEmptyEvent({required this.index}) : super();
}
