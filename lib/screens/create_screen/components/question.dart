import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder/helper/size_config.dart';
import 'package:form_builder/model/question_model.dart';
import 'package:form_builder/screens/components/custom_input_decoration.dart';
import 'package:form_builder/screens/create_screen/components/create_check_box.dart';
import 'package:form_builder/screens/create_screen/create_bloc.dart';
import 'package:form_builder/screens/create_screen/create_event.dart';
import 'package:form_builder/screens/create_screen/create_state.dart';
import 'package:form_builder/theme/color.dart';

class Question extends StatefulWidget {
  final int index;
  QuestionModel questionModel;

  Question({required this.index, required this.questionModel}) : super();
  @override
  _QuestionState createState() => _QuestionState();
}

class _QuestionState extends State<Question> {
  List<DropdownMenuItem<String>> answerOptions = [
    DropdownMenuItem(value: "1", child: Text("Text")),
    DropdownMenuItem(value: "2", child: Text("Number")),
    DropdownMenuItem(value: "3", child: Text("Toggle")),
    DropdownMenuItem(value: "4", child: Text("Checkbox"))
  ];
  List<int> emptyQuestion = [];
  List<int> emptyAnswerType = [];
  Widget selectedAnswerType = SizedBox.shrink();
  late CreateBloc _createBloc;
  final _createFormKey = GlobalKey<FormBuilderState>();

  @override
  void initState() {
    super.initState();
    _createBloc = BlocProvider.of<CreateBloc>(context);
  }

  void toggleSwitch(bool value) {
    widget.questionModel.isRequired = value;
    _createBloc
        .add(UpdateRequiredEvent(index: widget.index, isRequired: value));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: _createBloc,
      builder: (context, state) {
        return Card(
          child: FormBuilder(
            key: _createFormKey,
            autovalidateMode: AutovalidateMode.always,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  FormBuilderTextField(
                    name: 'question',
                    decoration: InputDecoration(labelText: 'Question'),
                    onSubmitted: (value) {
                      _createBloc.add(UpdateQuestionEvent(
                          index: widget.index, question: value));
                    },
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Visibility(
                        visible: emptyQuestion.indexOf(widget.index) != -1,
                        child: Text(
                          "Please set the question",
                          style: TextStyle(color: Colors.red),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: FormBuilderDropdown(
                      name: 'answer',
                      hint: Text('Answer Type'),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                      items: answerOptions,
                      decoration: customInputDecoration(""),
                      onChanged: (value) {
                        switch (value) {
                          case "1":
                            selectedAnswerType = SizedBox.shrink();
                            break;

                          case "2":
                            selectedAnswerType = SizedBox.shrink();
                            break;

                          case "3":
                            selectedAnswerType = SizedBox.shrink();
                            break;

                          case "4":
                            selectedAnswerType = CreateCheckBoxRow(
                              questionIndex: widget.index,
                            );
                            break;

                          default:
                            break;
                        }
                        _createBloc.add(UpdateAnswerTypeEvent(
                            index: widget.index, answerType: value.toString()));
                        setState(() {
                          selectedAnswerType = selectedAnswerType;
                        });
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Visibility(
                        visible: emptyAnswerType.indexOf(widget.index) != -1,
                        child: Text(
                          "Please choose an Answer type for this question",
                          style: TextStyle(color: Colors.red),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: selectedAnswerType,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text("Required"),
                          Switch(
                            onChanged: toggleSwitch,
                            value: widget.questionModel.isRequired,
                            activeColor: kPrimaryColor,
                            activeTrackColor: kSecondaryColor,
                            inactiveThumbColor: Colors.grey[100],
                            inactiveTrackColor: Colors.grey[100],
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          // GestureDetector(
                          //     child: Icon(
                          //   Icons.copy,
                          //   size: SizeConfig.safeBlockHorizontal * 6,
                          // )),
                          Container(width: 10),
                          GestureDetector(
                            onTap: () {
                              print("index: " + widget.index.toString());
                              _createBloc.add(
                                  RemoveQuestionEvent(index: widget.index));
                            },
                            child: Icon(
                              Icons.delete,
                              size: SizeConfig.safeBlockHorizontal * 6,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
      listener: (BuildContext context, Object? state) {
        if (state is QuestionIsEmptySuccess) {
          _createFormKey.currentState!.validate();
          // setState(() {
          //   emptyQuestion = List.from(state.index);
          // });
          print("index: " + state.index.toString());
        } else if (state is AnswerTypeIsEmptySuccess) {
          _createFormKey.currentState!.validate();
          // setState(() {
          //   emptyAnswerType = List.from(state.index);
          // });
        }
      },
    );
  }
}
