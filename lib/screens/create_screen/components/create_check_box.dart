import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder/helper/size_config.dart';
import 'package:form_builder/screens/components/custom_input_decoration.dart';
import 'package:form_builder/screens/create_screen/create_bloc.dart';
import 'package:form_builder/screens/create_screen/create_event.dart';
import 'package:form_builder/screens/create_screen/create_state.dart';
import 'package:form_builder/theme/color.dart';

class CreateCheckBoxRow extends StatefulWidget {
  final questionIndex;
  const CreateCheckBoxRow({required this.questionIndex}) : super();

  @override
  _CreateCheckBoxRowState createState() => _CreateCheckBoxRowState();
}

class _CreateCheckBoxRowState extends State<CreateCheckBoxRow> {
  List<Widget> addedCheckBox = [];
  late CreateBloc _createBloc;

  @override
  void initState() {
    super.initState();
    _createBloc = BlocProvider.of<CreateBloc>(context);
  }

  _removeCheckboxRow(int index) {
    setState(() {
      addedCheckBox.removeAt(index);
    });
  }

  _createCheckboxRow(int questionIndex, int checkBoxRowIndex) {
    setState(() {
      addedCheckBox.insert(
        checkBoxRowIndex,
        Row(
          children: [
            Container(
              width: SizeConfig.blockSizeHorizontal * 10,
              child: FormBuilderCheckbox(
                name: 'create_check_box',
                initialValue: false,
                onChanged: (newValue) {
                  setState(() {});
                },
                title: Text(""),
              ),
            ),
            Expanded(
              child: FormBuilderTextField(
                name: 'check_box_title',
                decoration: customInputDecoration("Option"),
                onSubmitted: (value) {
                  _createBloc.add(UpdateCheckBoxEvent(
                      questionIndex: widget.questionIndex,
                      checkBoxRowIndex: checkBoxRowIndex,
                      checkBoxRowText: value));
                },
              ),
            ),
            Container(
                width: SizeConfig.blockSizeHorizontal * 10,
                child: GestureDetector(
                    onTap: () {
                      _removeCheckboxRow(addedCheckBox.length - 1);
                    },
                    child: Icon(Icons.close)))
          ],
        ),
      );
    });
    _createBloc.add(CreateCheckBoxRowEvent(
        questionIndex: widget.questionIndex,
        checkBoxRowindex: checkBoxRowIndex,
        checkBoxText: ""));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: _createBloc,
      builder: (context, state) {
        return Column(
          children: [
            Column(
              children: addedCheckBox,
            ),
            GestureDetector(
              onTap: () {
                _createCheckboxRow(widget.questionIndex, addedCheckBox.length);
              },
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Add new checkbox",
                  style: TextStyle(color: kSecondaryColor),
                ),
              ),
            ),
          ],
        );
      },
      listener: (BuildContext context, Object? state) {},
    );
  }
}
